# multiwhip
A simple wrapper for the cd ripper _whipper_.

It executes whipper and repeats every time a new disc gets inserted, so
the user doesn't have to start whipper for each disc seperately.

Feel free to use and modify the program as you wish.