#!/bin/python3

import os
import time

# -- Settings
# Command or path for the whipper executable
whipper_path = 'whipper'
# Command line arguments for whipper
whipper_args = 'cd rip'
# Path to the disk drive (not /dev/...), if in doubt, whipper should output the correct path
disk_path = '/dev/sr0'
# Time in seconds to wait after ripping one disc, before watching out for the next disc
# (The mtime of the disc drive may change again a few seconds after ejecting the disc)
post_rip_delay = 10
# -- Program
# Start by asking for the first disc
input('Please insert the first disk and press enter.')

# Execute whipper until user interrupts
while True:
    try:
        # run whipper
        print('Executing whipper...')
        os.system('%s %s' % (whipper_path, whipper_args))
        print('Whipper has finished. Please insert the next disc or press CTRL-C to abort.')
        print('Only close the lid after you were asked to.')
        # Wait some time before listening for a new disc, because the mtime may change again a few seconds after the
        # disc got ejected
        time.sleep(post_rip_delay)
        print('Please now close the lid.')
        # wait until the last-modified time of the disc path increases to wait for the next disc
        disk_lastmodified = os.path.getmtime(disk_path)
        while not os.path.getmtime(disk_path) > disk_lastmodified:
            time.sleep(0.1)
        print('Disc inserted.')
    except KeyboardInterrupt:
        break
print('It was an honor to serve you.')